var indexSectionsWithContent =
{
  0: "cdfilmnosvw",
  1: "sw",
  2: "dm",
  3: "fms",
  4: "cilnov",
  5: "w",
  6: "l",
  7: "i",
  8: "d"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros"
};

