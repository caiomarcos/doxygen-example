Este projeto

Este projeto contém aqruivos fonte (doxygenExample.h,  doxygenExample.c e main.c), devidamente comentados, e a documentação gerada a partir deles. Utilize-os para modificar e testar funcionalidades Doxygen e gerar nova documentação.
Leia os arquivos fonte, entenda os comentários em formato Doxygen, gere a documentação e veja o que foi criado na documentação a partir de cada comentário.
Este README.md é o guia que deve ser seguido para que se utilize corretamente o Doxygen e se gere os arquivos de documentação desejados.

Introdução

O Doxygen é um programa que 'lê' arquivos fonte e gera uma documentação deles.

A documentação criada pelo Doxygen é prioritariamente para esclarecer funcionalidades para USUÁRIOS, e não para desenvolvedores. Assim, este documento deve documentar a interface - entradas, saídas, condições, exemplos de utilização etc. Portanto, detalhes da estrutura e funcionamento interno do programa e das funções não são documentados no formato Doxygen, e sim são comentados utilizando comentários regulares.

Comentando o código

Para o Doxygen entender os comentários e gerar a documentação a partir deles, é preciso que estes comentários sejam feitos em um padrão específico. Dentre as várias possibilidades, foi escolhido o padrão 'Qt style', utilizando o 'auto briefing' (maiores detalhes mais a frente). Neste formato, os comentários devem começar com /*! e terminar com */. Tudo que estiver entre estes marcadores será lido e processado pelo Doxygen. Este corpo de comentários irá se referir automaticamente ao símbolo do código que vem imediatamente depois - seja uma função, uma variável, um define, uma struct etc.

Dentro deste corpo de comentários sobre um símbolo, existem basicamente dois tipos de informação. 
A primeira é uma informação breve (brief) - é a primeira frase depois do marcador /*!, terminando no primeiro ponto final. Deve ser uma frase simples e breve descrevendo rapidamente o que é o símbolo que está sendo documentado. 
A segunda é uma descrição mais elaborada. Essa descrição é tudo que segue depois do primeiro ponto final até o marcador */, que termina o corpo de comentário Doxygen. Nesta parte descreve-se com mais detalhe o símbolo de código, usando marcadores. Por exemplo, em funções, utiliza-se os marcadores \param e \return para descrever os parâmetros e o retorno de tal função. Existem inúmeros outros marcadores que podem ser utilizados como \bug, \warning etc que podem ser utilizados a critério do programador (lista de marcadores/comandos em https://www.stack.nl/~dimitri/doxygen/manual/commands.html).

Estes comentários padrão Doxygen devem ser feitos na DECLARAÇÃO dos símbolos (funções, variáveis, enum etc), o que normalmente é feito nos arquivos .h (mas não somente). Caso a função seja definida direto, sem declaração, ela deve então ser comentada, da mesma maneira, na definição. Atenção para não fazer comentários Doxygen de funções duas vezes, isto é, na declaração em um arquivo .h e na definição em um arquivo .c, pois isto gerará resultados indesejados na documentação.

Algumas regras:
Todos arquivos devem incluir o marcador /*!\file */ em seu início. Arquivos que não contenham este marcador serão ignorados pelo Doxygen.
Todas estruturas de dados globais devem ser documentadas, por exemplo classes, structs, enums, typedefs.
Itens internos às estruturas podem ou não ser documentados (enumeradores de um enum, ou variáveis internas a uma struct).
Todas funções devem ser documentadas, explicitando parâmetros e retorno, caso existam.
Todas variáveis globais devem ser documentadas.
Todos defines e macros globais devem ser documentados.
Includes podem ou não ser documentados.


Itens locais (variáveis, estruturas, rotinas, instruções) não são documentadas. O Doxygen NÃO DOCUMENTA CÓDIGO INTERNO A FUNÇÕES. Comentários padrão Doxygen feitos dentro de funções irão aparecer na documentação dentro da descrição da função, ignorando os itens a quais eles descrevem diretamente.

Executando o Doxygen e gerando a documentação

Feitos os comentários em formato Doxygen necessários nos arquivos fonte, é necessário instalar, configurar e executar o Doxygen.

Windows

Fazer download do instalador em http://www.stack.nl/~dimitri/doxygen/download.html (doxygen-*-setup.exe).
Executar instalador e seguir intalação normalmente.

Executar Doxywizard, que abrirá uma janela. O primeiro passo é selecionar a pasta na qual o Doxygen irá rodar - escolher a pasta do projeto que contenha os arquivos a serem documentados.
Depois, deve-se configurar algumas opções do Doxygen.
Primeiramente, na aba WIZARD (já selecionada ao iniciar o programa), deve-se configurar os seguintes campos:
- No tópico 'project':
	- Project name - preencher com nome do projeto;
	- Project synopsis - preencher com breve descrição do projeto;
	- Scan recursively (marcar);
- No tópico 'mode':
	- documented entities only (marcar);
	- optimize for C or PHP output (marcar);
-No tópico 'output':
	- HTML (marcar)
	- plain HTML (marcar)
	- with search function (marcar);
	- Desmarcar LaTeX.
- No menu "diagrams":
	- use built-in class diagrams (marcar).

Depois de preenchido e marcados as devidas configurações, deve-se selecionar a aba 'EXPERT' e fazer as seguintes modificações:
- No menu 'Project':
	- Marcar QT_AUTOBRIEF.
- No menu 'Build'
	- Marcar EXTRACT_PRIVATE;
	- Marcar EXTRACT_STATIC;

Depois de configurado os itens acima, deve-se selecionar a aba 'RUN', e clicar em 'Run doxygen'. Pronto, todos os arquivos de documentação serão gerados.
Vale a pena ler o log e observar qualquer erro ou aviso, por exemplo itens que não foram documentados ou erros que impedem a geração dos documentos.


