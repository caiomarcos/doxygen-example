/*! \file */
#include "doxygenExample.h"

/* Do not write doxygen documentation here. It will mess up comments made in the
.h file.
*/
int function_1 (int intParam, char charParam)
{
  /*! This comment inside the function implementation will show inside the
  function details in doxygen, but they should be avoided. Comments to aid
  development are fine and should be made with regular comments, not with doxygen
  style. Comments for user documentation should be done in doxygen style in
  the .h file.
  */
  /*! It's not possible to document entities inside function implementations.
  Therefore, this comment will show as a general comment, just as the one above,
   and not as a documentation of the result variable. */
  int result;
  result = intParam + charParam;
  return result;
}

void simpleFunction()
{
 /* Regular comment to aid development, not documented, won't show up in doxygen
 documents. */
}

double function_2 ( )
{

}

whateverType function_0(int a, int b)
{

}
