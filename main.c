/*! \file */
#include "doxygenExample.h"

/*! A number used here.
  It is a global variable, so must be described.
*/
int number;

/*! Function without declaration, only definition.
  Functions like this should also be documented, but since they have no
  declaration, they must be documented wherever they are defined.
  \param distance Distance in meters.
  \param seconds Time in seconds.
  \return Speed in m/s.
*/
void speed(int distance, int seconds)
{
  return distance/seconds;
}

/*! Brief description of main().
  Describe main() in more details.
  \return Should not return.
*/
int main()
{
  /*! Items inside function implementations are not documented. This comment
  will show as a general comment inside the details of main().*/
  int localNumber;
  char localChar;
}
