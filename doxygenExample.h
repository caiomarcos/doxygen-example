/*! \file */

#ifndef _DOXYGEN_EXAMPLE_H
#define _DOXYGEN_EXAMPLE_H

#include <system_file1.h>
#include <system_file2.h>
#include "local_file.h"

/*! Brief description of a #define.
   More detailed description. Not all #define require a comment, only those
   deemed relevant.
 */
#define DEFINITION

/*! Brief description of a variable. All global variables need this. */
uint16_t variable1 = 0x0001;

/*! Only a brief description.*/
uint16_t variable2 = 0b10;

/*! A brief description of typdef struct.
 * More complete description. All structs need comments.
 */
typedef struct whateverType
{
    char letter; /*!< Post-comment to members of struct. */
    char options; /*!< Options description. */
    char length; /*!< Length. */
    uint16_t ID; /*!< ID description. */
}whateverType;

/*! Simple struct. */
struct simpleStruct
{
    char a;
    char b;
    char c; /*!< Not all members of struct need comments. */
};

/*! Brief description of an \c enum.
  More elaborated description of this enum.
*/
enum list {
 item1, /*!< First item. */
 item2 = 10, /*!< Second item equals 10. */
 itemN = NULL,  /*!< Item N is null */
};

/*! Another global variable.
   Uses a typedef, but still is a global variavel, so still needs a comment.
*/
whateverType instanceOfWhatever;

/*! Brief description of a function.
  More detailed description of said function. Every function need this.
  If function has parameters, their descriptions are mandatory.
  If it returns anything, return description is mandatory.
  \param intParam Integer parameter.
  \param charParam Parameter that is a character.
  \return Whatever the function is returning.
*/
int function_1 (int intParam, char charParam);

/*! Simple description of a simple function.*/
void simpleFunction();

/*! More info about function documentation.
  Functions must be described in here.
  <b>COMMENTS INSIDE THE DEFINITIONS OF FUNCTIONS IN .C FILES DO NOT MATTER FOR
  DOXYGEN DOCUMENTATION.</b>
  \return Whatever the function is returning, as a double.
*/
double function_2 ( );

/*! Formating examples.
  A word of code can also be inserted like \c this which is equivalent to
  <tt>this</tt> and can be useful to say that the function returns a \c void or
  \c int. If you want to have more than one word in typewriter font, then just
  use \<tt\>. We can also include text verbatim, \verbatim like this\endverbatim
  Sometimes it is also convenient to include an example of usage:
  \code
  whateverType *variableOfTypeWhatever = function_0(param1, param2);
  printf("something...\n");
  \endcode
  <b>This is how you write bold text</b> or, if it is just one word, then you can
  just do \b this.
  \note Something to note.
  \warning Warning. Does not return.
*/
whateverType function_0(int a, int b);
